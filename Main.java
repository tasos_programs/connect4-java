package com.connect4;
import java.util.Scanner;

public class Main {

  static void MainGameLoop()
  {
    // Main loop consists of: Get user input, modify game's play space,
    // show modified play space and check if any of the two players won.
    Board board = new Board();
    Game game = new Game();

    String playSpace = board.CreatePlaySpace(); 
    char playerTurn = 'x';
    while (true) 
     {     
      board.PrintPlayspace(playSpace);
      int userInput;
      // Keep asking for input if first the time is invalid
      do
      {
        userInput = game.GetUserInput();
      } while (game.IsInvalidInput(userInput, playSpace));

      playSpace = board.ModifyPlayspace(playSpace,userInput,playerTurn);
      if (game.CheckWinConditions (playSpace ))
      {
        board.PrintPlayspace(playSpace);
        System.out.println("\nPlayer "+ playerTurn+" wins");
        return;
      }
      playerTurn = (playerTurn == 'x') ? 'o' : 'x'; // next player's turn   
    }
  }

  public static void main(String[] args)
  {    
    while (true)
    {
     MainGameLoop();
     String newGameAnswer = Game.NewGameQuestion();
     if ( newGameAnswer.charAt(0) == 'n')
     {
       break;
     }
    }
  }
}

class Board
{
  static String CreatePlaySpace() 
  {
    String playSpace = "";
    // Adds 20 '-' to a new string, then returns it
    for (int i = 0; i < 20; i++) {
    playSpace = playSpace.concat(" ");
    }
    return playSpace;
  }

  static void PrintPlayspace(String playSpace)
   {
      // uses a combination of print and println to split the string into a 5x4 board      
      for (int i = 0; i < 20; i++)
      {
        if ((i+1) %5 ==0) // positions 5,10 and 15 (+1 because i=0)
        {
          System.out.println("("+playSpace.charAt(i) + ")");
        } 
        else
        {
          System.out.print("("+playSpace.charAt(i)+ ")");
        }
      }
      System.out.println(" =  =  =  =  =\n 1  2  3  4  5\n");
  }

  static String ModifyPlayspace(String playSpace, int location, char characterToAdd)
   {
     // location is a range from 0-4 (positions 1-5)
     // each can have up to 4 possible positions, from top to bottom.
     // Search for the last occurance of "-" and only replace that.
     // to do that, add "5" 3 times in (int) location
     int last_Successful_location = 0;
     for (; location < 20; location+=5)
     {
        char characterAtLocation = playSpace.charAt(location); // easier to read
        // if the location there unoccupied, that's the one we need to edit (last location assigned from loop)
        if (characterAtLocation == ' ')
        {
          last_Successful_location = location; 
        }
     }     
      // now we need to modify our playSpace string to include our modification
      StringBuilder newPlaySpaceBuilder = new StringBuilder(playSpace);
      newPlaySpaceBuilder.setCharAt(last_Successful_location,characterToAdd);          
      return newPlaySpaceBuilder.toString();
   }
}
class Game
{
    
  static String NewGameQuestion()
  {
    Scanner input = new Scanner(System.in);
    while (true)
    {
      try
      {
        System.out.print("\nNew game? [y/n]: ");
        String answer = input.next();
        answer = answer.toLowerCase();
        if (answer.charAt(0) != 'y' && answer.charAt(0) != 'n')
        {     
        continue;
        }
        return answer;
      }
      catch (java.util.InputMismatchException e)
      {    
        continue;
      }
    }
  }

  static int GetUserInput() 
  {
    while (true) // better than returning the same function (memory wise)
    {
      Scanner input = new Scanner(System.in);
      try 
      {
        System.out.print("Enter Number [1-5]: ");
        int number = input.nextInt(); // this is where the exception can happen
        return number-1;
      } 
      catch (java.util.InputMismatchException e) 
      {
        continue; // continue until we get a valid input
      }
    }
  }

  static boolean IsInvalidInput(int input, String playSpace) 
  {
    // checks if the position the user specified is empty (' ')
    // we also want inputs from 0 to 4. Nothing bigger (messes with placement later)
    if ( input >= 0 && input <5 && playSpace.charAt(input) == ' ')
    {
      return false;
    }
    return true;
  }

 static boolean CheckLine(String line)
 {
   if (line.contains("xxx") || line.contains("ooo"))
   {
     return true;
   }
   return false;
 }
   static boolean CheckHorizontalWinCon(String playSpace)
   {
     // every 5 spaces, check occuring string for 3* x or o
     String line;
     int startIndex = 0;
     for (int i=0; i<=20; i+=5)
     {     
       line = playSpace.substring(startIndex,i);    
       if (CheckLine(line)){return true;}
       startIndex = i; // start from the previous position
       }
     
     return false;
   }
  
   static boolean CheckVerticalWinCon(String playSpace)
   {
     // loop 5 times, each time adding +5 to substr
     for (int i =0; i<5; i++)
     {      
       StringBuilder line = new StringBuilder();
       
       for (int b =i; b < 20; b+=5) // takes our current position (i) and adds 5, effectively going down one line 
       {
         line.append(playSpace.charAt(b));
       }
       
       if (CheckLine(line.toString())){return true;}
     }
     return false;
   }
   
  static boolean CheckDiagonalWinCon (String playSpace)
  {    
    StringBuilder line = new StringBuilder();
      for (int i =0; i<8; i++) // top left to bottom right
        {            
          for (int b =i; b < 20; b+=6) // add 6 so we go to the next diagonal position      
          {
            line.append(playSpace.charAt(b));
          }
            if (CheckLine(line.toString())){return true;}
        }

    line.setLength(0); //reset line

      for (int i =4; i>0; i--) // top right to bottom left, to cover all angles. (i 0-4 represents the positions per horizontal line)
      {            
        for (int b =i; b <20; b+=4) // add 4 since we're going backwards     
        {
          line.append(playSpace.charAt(b));
        }
        
        if (CheckLine(line.toString())){return true;}
      }
    
      return false;
  }
   
   static boolean CheckWinConditions(String playSpace)
   {
     return(CheckHorizontalWinCon(playSpace) || CheckVerticalWinCon(playSpace) || CheckDiagonalWinCon(playSpace));
   }   
}
